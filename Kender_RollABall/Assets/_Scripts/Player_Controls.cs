﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Player_Controls : MonoBehaviour
{
    public float speed; //created speed float for the ball to roll
    public Text countText; //score text
    public Text winText;// you win text
    public Text LostText; //Lost text

    // Creating public variables for the texts in the game
    Rigidbody rb;
    private int count;


    bool braking; //true or false is the player braking 
    void Start()
    {
        rb = GetComponent<Rigidbody>();// Renamed the rigid body

        count = 0; //set score at beginning
        SetCountText(); //created the count text for scipt
        winText.text = ""; // wintext is blank until script later
        LostText.text = ""; //lost text is blank till script later
    }
    
    private void Update()
    {
        braking = Input.GetKey(KeyCode.Space); // braking is set to when the space bar is pressed
    }

    //Below is the ball movement script
    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal"); //which direction affects which player movement
        float moveVertical = Input.GetAxis("Vertical"); //which direction affects which player movement

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical); //movement is only on the x and z so the player doesnt fly

        rb.AddForce(movement * speed);//the force added to the rigidbody is its movement times speed
        { if (Input.GetKey(KeyCode.Return))// if the return key pressed...
            {
                Application.LoadLevel("Play Area"); //...restart the level

            }
        }
        if (braking)
        {
            float brakeAmount = .5f;// when space is pressed the ball with slow it's velocity by 15% 
            rb.velocity = new Vector3(rb.velocity.x * brakeAmount, rb.velocity.y, rb.velocity.z * brakeAmount);
        }
    }

    // Below is for the player to pick up the cubes in the game
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up")) //if the player collides with a pick up
        {
            other.gameObject.SetActive(false); // set the pick up to disappear 
            count = count + 1; //increase the score by 1
            SetCountText();
        }
        {
            if (other.gameObject.CompareTag("BoosterPad")) // If player connects with a boosterpad...
            {
                rb.velocity = rb.velocity * 4f; // ....then mulitple its velocity by 4
            }
        }
        {
            if (other.gameObject.CompareTag("InnerWall"))// if player hits a wall..
            {
                LostText.text = "You Lose!!";//text on screen displayed you lose
                Time.timeScale = 0f; //in game time is stopped when you lose
            }

        }
    }
    void SetCountText() // setting up the win text after collecting all the pick ups
    {
        countText.text = "Count: " + count.ToString();
        if (count > 13)
        {
            winText.text = "You Win!!"; //say you win once all pick have been collected

        }
    }
}
